[![pipeline status](https://gitlab.com/nobodyinperson/harbour-sailcash/badges/master/pipeline.svg)](https://gitlab.com/nobodyinperson/harbour-sailcash/commits/master)
[![latest rpm on master](https://img.shields.io/badge/rpm-latest on master-brightgreen.svg)](https://gitlab.com/nobodyinperson/harbour-sailcash/-/jobs/artifacts/master/browse?job=rpmbuild)


# SailCash

This **will** be an expenses tracking app for SailfishOS someday.

## Development

To set up the build environment, clone the repository and then run once from
the repository root:

```bash
git submodule update --init --recursive
autoreconf --install
./configure
```

This produces all the necessary Makefiles and the `control` Makefile.

### Building the RPM package

```bash
./control rpm
```

## Testing the package on the device

This is implemented via SSH/SCP. Set the parameter `device_ssh_host` to the
hostname of your Sailfish device that you have network access to. This also
works for the SailfishOS Emulator. It is recommended that you specify the SSH
settings in `.ssh/config`.

In all of the following commands, you can always hand the `-n` parameter to
check what the command would do without executing anything.

### Deploying the RPM package to a device

To (build and) deploy the rpm to the device via `scp`, run

```bash
./control deploy device_ssh_host=jolla
```

### Installing the RPM on the device

To (build, deploy and) install the RPM on the device, run

```bash
./control install-device device_ssh_host=jolla
```

If you get an error like `Failed to obtain authentication`, you can use
`install_precmd=devel-su` (or `install_precmd=sudo` if you have `sudo`
configured) to run the installation with root privileges:

```bash
./control install-device device_ssh_host=jolla install_precmd=sudo
```

### Running the app on the device

To (build, deploy, install and) run the application on the device, run

```bash
./control run device_ssh_host=jolla
```

It might be necessary to source your shell configuration. For BASH, specify
`run_precmd=". ~/.bashrc &&"`
and for ZSH, specify `run_precmd=". ~/.zshrc &&"`:

```bash
./control run device_ssh_host=jolla run_precmd=". ~/.zshrc &&"
```

If you want to run the application with a different language, e.g. French,
append `LANG=fr_FR.utf8` to the `run_precmd=` parameter.

On a Jolla 1 (configured in `.ssh/config` as `jolla`) with `zsh` as shell, the
oneliner to test the app should read:

```bash
./control run device_ssh_host=jolla install_precmd=devel-su run_precmd=". ~/.zshrc &&"
```
