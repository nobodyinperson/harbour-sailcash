#!/usr/bin/env python3
import argparse
import os
import sys
import logging
import time

from selenium.webdriver import Firefox
from selenium.common.exceptions import NoSuchElementException

parser = argparse.ArgumentParser(description="Deploy an RPM to OpenRepos.net")
parser.add_argument("-n","--appname", required = True,
    help="Application name on OpenRepos.net.")
parser.add_argument("-u","--username",help="Your OpenRepos.net username. "
    "Defaults to OPENREPOS_USERNAME environment variable.",
    default = os.environ.get("OPENREPOS_USERNAME"))
parser.add_argument("-p","--password",help="Your OpenRepos.net password. "
    "Defaults to OPENREPOS_PASSWORD environment variable.",
    default = os.environ.get("OPENREPOS_PASSWORD"))
parser.add_argument("-r","--rpm",help="The RPM you want to upload",
    required=True)
parser.add_argument("-v","--verbose",help="verbose output",action="store_true")

args = parser.parse_args()

if not args.username:
    logging.error("No username given!")
    parser.print_help()
    sys.exit(1)
if not args.password:
    logging.error("No password given!")
    parser.print_help()
    sys.exit(1)

loglevel = logging.INFO
loglevel = logging.DEBUG if args.verbose else loglevel
logging.basicConfig(level = loglevel)

logging.info("Opening Firefox...")
firefox = Firefox()
logging.info("Firefox was opened.")

def quit(exitcode=0):
    logging.info("Closing Firefox...")
    firefox.close()
    logging.info("Firefox closed.")
    sys.exit(exitcode)

logging.info("Loading openrepos.net...")
firefox.get("https://openrepos.net")
logging.info("openrepos.net was loaded.")

logging.info("Typing username...")
firefox.find_element_by_id("edit-name").send_keys(args.username)
logging.info("Username was typed.")

logging.info("Typing password...")
firefox.find_element_by_id("edit-pass").send_keys(args.password)
logging.info("Password was typed.")

logging.info("Logging in...")
firefox.find_element_by_id("edit-submit--3").click()
logging.info("Logged in.")

logging.info("Going to 'My Applications' page...")
firefox.find_element_by_link_text("My Applications").click()
logging.info("Now on 'My Applications' page.")

logging.info("Going to app '{}' page...".format(args.appname))
try:
    firefox.find_element_by_partial_link_text(args.appname).click()
except NoSuchElementException:
    logging.error("Could not find application '{}'".format(args.appname))
    quit(1)
logging.info("Now on app '{}' page.".format(args.appname))

logging.info("Going to Edit Tab...")
firefox.find_element_by_link_text("Edit").click()
logging.info("Now on Edit Tab.")

logging.info("Checking if the same rpm exists already...")
rpmname = os.path.basename(args.rpm)
try:
    firefox.find_element_by_link_text(rpmname)
    logging.error("The rpm file '{}' is already there! Stopping!"\
        .format(rpmname))
    quit(2)
except NoSuchElementException:
    pass

rpm = os.path.abspath(args.rpm)
logging.info("Selecting rpm file '{}' for upload...".format(rpm))
firefox.find_elements_by_class_name("form-file")[1].send_keys(rpm)
logging.info("rpm file '{}' selected for upload...".format(rpm))

logging.info("Uploading rpm file '{}'...".format(rpm))
firefox.find_elements_by_css_selector("input[value=Upload]")[1].click()
logging.info("rpm file '{}' uploaded.".format(rpm))
time.sleep(1)
logging.info("Saving...")
firefox.find_element_by_css_selector("input[value=Save]").click()
logging.info("Saved!")

quit()
