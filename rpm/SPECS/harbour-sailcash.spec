# Prevent brp-python-bytecompile from running.
%define __os_install_post %{___build_post}

# "Harbour RPM packages should not provide anything."
%define __provides_exclude_from ^%{_datadir}/.*$

Name: harbour-sailcash
Version: 0.1
Release: 1
Summary: Simple expenses tracking application
License: GPLv3+
URL: https://gitlab.com/nobodyinperson/sailcash
Source: %{name}-%{version}.tar.xz
Packager: Yann Büchau <nobodyinperson@gmx.de>
Conflicts: %{name}
BuildArch: noarch
BuildRequires: gettext
BuildRequires: inkscape
BuildRequires: make
BuildRequires: qt5-qttools-linguist
Requires: pyotherside-qml-plugin-python3-qt5 >= 1.2
Requires: python3-base
Requires: sailfishsilica-qt5

%description
Track your daily expenses on your SailfishOS device for later import into more
sophisticated programs like GnuCash or a spreadsheet program.

%prep
%setup -q

%install
./configure --prefix=/usr
make
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.svg
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/qml/*.qml
%{_datadir}/%{name}/python/*.py
%{_datadir}/%{name}/translations/*.qm
%{_datadir}/%{name}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Tue Mar 06 2018 Yann Büchau <nobodyinperson@gmx.de> 0.1-1
- Initial RPM release
