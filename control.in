#!@make_bin@ -f
rpm_dir = rpm
rpm_build_dir = $(rpm_dir)/BUILD
rpm_buildroot_dir = $(rpm_dir)/BUILDROOT
rpm_sources_dir = $(rpm_dir)/SOURCES
rpm_specs_dir = $(rpm_dir)/SPECS
rpm_rpms_dir = $(rpm_dir)/RPMS

rpm_dirs = $(rpm_build_dir) $(rpm_specs_dir) $(rpm_sources_dir) \
	$(rpm_buildroot_dir) $(rpm_rpms_dir)

specfile = $(rpm_specs_dir)/@PACKAGE_NAME@.spec
rpmfile = $(rpm_rpms_dir)/noarch/@PACKAGE_NAME@-@PACKAGE_VERSION@-1.noarch.rpm

contentdirs = qml desktop icons man locale python
content = $(shell @find_bin@ $(contentdirs)) $(specfile)

.PHONY: rpm
rpm: $(rpmfile)

$(rpmfile): $(content) | $(rpm_sources_dir)
	@rm_bin@ -rf $(rpm_build_dir)/@PACKAGE_NAME@-@PACKAGE_VERSION@
	@rm_bin@ -rf $(rpm_buildroot_dir)/@PACKAGE_NAME@-@PACKAGE_VERSION@*
	$(MAKE) dist
	cp @PACKAGE_NAME@-@PACKAGE_VERSION@.tar.xz $(rpm_sources_dir)
	@rpmbuild_bin@ -D '_topdir $(abspath $(rpm_dir))' -ba --nodeps $(rpmbuild_bin) $(specfile)
	@if test -f $(rpmfile);then \
		echo "RPM package '$(rpmfile)' was built successfully.";\
	else \
		echo "RPM package '$(rpmfile)' was not built!";\
		exit 1;\
	fi

$(rpm_dirs): % :
	@mkdir_bin@ -p $@

device_ssh_host=sailfish
device_deploy_dir=~/Downloads
deploy_rpm=$(rpmfile)
run_precmd = . ~/.zshrc >/dev/null &&
install_precmd=

.PHONY: deploy
deploy: $(rpmfile)
	@scp_bin@ $(deploy_rpm) $(device_ssh_host):$(device_deploy_dir)

.PHONY: install
install-device: deploy
	@ssh_bin@ -t $(device_ssh_host) '$(if $(install_precmd),$(install_precmd) )pkcon -y install-local $(device_deploy_dir)/$(notdir $(deploy_rpm))'

.PHONY: run
run: install-device
	@ssh_bin@ -t $(device_ssh_host) '$(if $(run_precmd),$(run_precmd) )sailfish-qml @PACKAGE_NAME@'

.PHONY: clean-build
clean-build:
	@rm_bin@ -rf $(rpm_build_dir)/@PACKAGE_NAME@-@PACKAGE_VERSION@
	@rm_bin@ -rf $(rpm_buildroot_dir)/@PACKAGE_NAME@-@PACKAGE_VERSION@*

.PHONY: clean
clean: clean-build
	@rm_bin@ -rf $(rpmfile)
