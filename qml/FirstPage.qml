/* -*- coding: utf-8-unix -*-
 *
 * Copyright (C) 2018 Yann Büchau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "."

Page {

    id: page
    property bool downloading: false

    PageHeader {
        id: header
        width: parent.width
        title: "Colorology app"
    }

    Label {
        id: mainLabel
        anchors.verticalCenter: parent.verticalCenter
        text: "Color is unknown."
        visible: !page.downloading
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ProgressBar {
        id: dlprogress
        label: "Downloading latest color trends."
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width
        visible: page.downloading
    }

    Button {
        text: "Update color"
        enabled: !page.downloading
        anchors.bottom: parent.bottom
        width: parent.width
        onClicked: {
            python.startDownload();
        }
    }

}


