/* -*- coding: utf-8-unix -*-
 *
 * Copyright (C) 2018 Yann Büchau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "."

CoverBackground {
    Label {
        id: label
        anchors.centerIn: parent
        text: qsTranslate("cover","Plain Cover")
    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-next"
            onTriggered: python.call('python.coveractions.action_next', [], function(newstring) {
                label.text = newstring;
            });
        }

        CoverAction {
            iconSource: "image://theme/icon-cover-pause"
            onTriggered: python.call('python.coveractions.action_pause', [], function(newstring) {
                label.text = newstring;
            });
        }
    }


}

